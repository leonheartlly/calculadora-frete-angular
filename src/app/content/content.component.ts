import { Component, OnInit } from '@angular/core';
import Typed from 'typed.js';
import { Router } from '@angular/router';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  constructor( private router: Router) { }

  ngOnInit() {

    const options = {

      stringsElement: '#typed-strings',
      
      strings: ['Transformação é o que nos move', 'Controle sobre custos e planejamento', 'Melhoria no processo de decisão'],
      
      typeSpeed: 100,
      
      backSpeed: 100,
      
      backDelay: 200,
      
      smartBackspace: true,
      
      fadeOut: true,
      
      showCursor: false,
      
      startDelay: 1000,
      
      loop: true
      
      };

      const typed = new Typed('.typing-element', options);

      
  }

  linkCalculadora() {
    this.router.navigate(['calculadora'])
   }

 
   
}
