import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastroRegrasComponent } from './cadastro-regras.component';

describe('CadastroRegrasComponent', () => {
  let component: CadastroRegrasComponent;
  let fixture: ComponentFixture<CadastroRegrasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastroRegrasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastroRegrasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
