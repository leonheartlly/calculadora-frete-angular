import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-cadastro-regras',
  templateUrl: './cadastro-regras.component.html',
  styleUrls: ['./cadastro-regras.component.css']
})
export class CadastroRegrasComponent implements OnInit {

  public data: any;
  constructor(private http: HttpClient) { }

  ngOnInit() {
    let obs = this.http.get('https://marmita-uai.herokuapp.com/states');
    obs.subscribe( (response) => {
      this.data = response;
      console.log(response)
    } );
  }

  

}
