import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    console.log('Component app-header inciado...');
  }

  linkRegras(){
    this.router.navigate(['regras'])
  }

  linkCidades(){
    this.router.navigate(['cidades'])
  }

  linkVersoes(){
    this.router.navigate(['cidades'])
  }

  linkHome(){
    this.router.navigate(['']);
  }

}
