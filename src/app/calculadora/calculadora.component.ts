import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup,Validators , FormBuilder } from '@angular/forms';
import { AppComponent } from '../app.component';



import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-calculadora',
  templateUrl: './calculadora.component.html',
  styleUrls: ['./calculadora.component.css']
})



export class CalculadoraComponent implements OnInit {

  public data: any;
  public total: any;
  submitted = false;
  model : any = {};
  calculateForm: FormGroup;
  
  constructor(private http: HttpClient, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.calculateForm = this.formBuilder.group({
      destino:['', Validators.required],
      origem:['', Validators.required],
      peso:['', Validators.required]      
    })

    let obs = this.http.get('http://127.0.0.1:8090/cidades');
    obs.subscribe( (response) => {
      this.data = response;
      console.log(response)
    } );
  }

  get f() { return this.calculateForm.controls; }

  onSubmit(){
    this.submitted = true;

    if (this.calculateForm.invalid) {
      return;
    }


    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');

    let obs = this.http.post('http://127.0.0.1:8090/regras/calculate',this.calculateForm.value);
    obs.subscribe((response) => {
      this.total = response;
    })
  }
  
}



