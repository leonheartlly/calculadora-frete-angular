import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { HttpClient } from '@angular/common/http';


@Component({
selector: 'app-root',
templateUrl: './app.component.html',
styleUrls: ['./app.component.css']
})

export class AppComponent {
title = 'front-calculadora-transporte';

constructor(public dialog: MatDialog, private http: HttpClient) {}

openDialog() {
    const dialogRef = this.dialog.open( AppComponent, {
      height: '350px'
    });

  

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  ngOnInit(){
    
  }
  
}

