import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ContentComponent } from './../content/content.component';
import { CadastroRegrasComponent } from './../cadastro-regras/cadastro-regras.component';
import { CalculadoraComponent } from './../calculadora/calculadora.component';
import { CidadesComponent } from './../cidades/cidades.component';
import { RegrasComponent } from './../regras/regras.component';

const routes: Routes = [
  { path: 'cadastro-regras', component: CadastroRegrasComponent },
  { path: 'calculadora', component: CalculadoraComponent },
  { path: 'cidades', component: CidadesComponent },
  { path: 'regras', component: RegrasComponent },
  { path: '', component: ContentComponent }
  ];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports:[RouterModule]
})
export class AppRoutingModule { }
