# FrontCalculadoraTransporte

A aplicação foi gerada com [Angular CLI](https://github.com/angular/angular-cli) version 7.3.5.

## Development server

Para subir a aplicação, rode o comando 'ng s'. A seguir, acesse 'http://localhost:4200'.

## Build

Caso necessário, rode o comando 'ng build' para efetuar o build do projeto.

## Tecnologias

Para conseguir rodar o projeto, será necessário a instalação do 'node', e 'angular 6'